@Shop
Feature: As a potential client i need to navigate the Shop Page

  Background:
    Given The client is in Practice page
    And The client clicks on Shop Button

  Scenario Outline: The client adds item in Shop page to basket
    When The client adds item to basket
    And The client sees item and price in Menu item
    And The client clicks on the Menu Item
    And The client find total and subtotal
    And The client checks total is bigger that subtotal
    And The client clicks on Proceed to Checkout
    Then The client can see Billing Details,Order Details,Additional details and Payment gateway details
    And The client can fill in his details <firstname>,<lastname>,<email>,<phone>,<country>,<address>,<city>,<state>,<postcode>,<payment>
    And The client tries to <action>

    Examples:
      | firstname | lastname   | email                     | phone      | country   | address    | city     | state        | postcode | payment                 | action
      | Giuliana  | Castellini | gcastellini1994@gmail.com | 1567891200 | Argentina | Callao 970 | Recoleta | Buenos Aires | C1419    | PayPal Express Checkout | place order


  Scenario Outline: The client checks tax functionality
    When The client adds item to basket
    And The client sees item and price in Menu item
    And The client clicks on the Menu Item
    And The client find total and subtotal
    And The client checks total is bigger that subtotal
    And The client clicks on Proceed to Checkout
    Then The client checks tax for India is 2% and for <country> is 5%

    Examples:
      | country   |
      | Argentina |
