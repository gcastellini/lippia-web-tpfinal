@Login
Feature: As a potential client i need to log into the website

  Background:
    Given The client is in Practice page
    And The client clicks on My Account button

@Case
  Scenario Outline: The client fails to login with case changed username and password
    When The client enters email <email> and password <password>
    Then The client see error message for user <email>
    Examples:
      | email                           | password   |
      | GIULIANA_CASTELLINI@HOTMAIL.COM | crowdar22! |

  @Logout
  Scenario Outline: The client logs in and signs out of page
    When The client enters email <email> and password <password>
    And The client is logged in
    And The client signs out
    Then The client goes back and is not signed in

    Examples:
      | email                           | password   |
      | giuliana_castellini@hotmail.com | Crowdar22! |

  @Register
  Scenario Outline: The client registers with an email and empty password
    When The client registers an email <email> and no password
    Then An error message is displayed for email <email>
    
    Examples: 
    |email|
    |gcastellini1994@gmail.com|
    ||


  @Pass
  Scenario Outline: The client logs in and sees account details
    When The client enters email <email> and password <password>
    And The client is logged in
    And The client clicks on My Account and <button>
    Then The client sees they can change the password

    Examples:
      | email                           | password   | button|
      | giuliana_castellini@hotmail.com | Crowdar22! |Account Details|

  @Logout
  Scenario Outline: The client logs in and logs out
    When The client enters email <email> and password <password>
    And The client is logged in
    And The client clicks on My Account and <button>
    Then The client goes back and is not signed in

    Examples:
      | email                           | password   | button|
      | giuliana_castellini@hotmail.com | Crowdar22! |Logout|
