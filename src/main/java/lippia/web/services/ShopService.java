package lippia.web.services;

import com.crowdar.core.actions.ActionManager;

import com.crowdar.driver.DriverManager;
import lippia.web.constants.ShopConstants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;



public class ShopService extends ActionManager {

    public static void clickAddBasket(){
        click(ShopConstants.ADD_BASKET);
    }

    public static void checkMenuItem() throws InterruptedException {
        Thread.sleep(2000);
        String items = getText(ShopConstants.MENU_ITEM);
        String price = getText(ShopConstants.PRICE_ITEM);
        Assert.assertEquals(items,"1 Item");
        Assert.assertEquals(price,"₹500.00");

    }
    public static void fillIndia(){
        WebDriver driver = DriverManager.getDriverInstance();
        Actions action = new Actions(driver);
        getElement(ShopConstants.COUNTRY).sendKeys("India");
        driver.findElement(By.id("select2-drop-mask")).click();
        action.moveToElement(getElement(ShopConstants.TAX)).perform();
        final String indianTax = getText(ShopConstants.TAX).substring(1);
        Assert.assertEquals(indianTax,"10.00" );



    }
    public static void fillCountry(String country) throws InterruptedException {
        WebDriver driver = DriverManager.getDriverInstance();
        Actions action = new Actions(driver);
        action.moveToElement(getElement(ShopConstants.COUNTRY)).sendKeys(country).perform();
        driver.findElement(By.id("select2-results-1")).click();
        Thread.sleep(5000);
        action.moveToElement(getElement(ShopConstants.TAX)).perform();
        final String countryTax = getText(ShopConstants.TAX).substring(1);
        Assert.assertEquals(countryTax,"25.00" );
    }


}
