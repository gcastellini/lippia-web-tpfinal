package lippia.web.services;

import com.crowdar.core.PropertyManager;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import io.cucumber.java.eo.Se;
import lippia.web.constants.HomeConstants;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import java.util.ArrayList;
import java.util.List;

import static com.crowdar.core.actions.WebActionManager.navigateTo;

public class HomeService extends ActionManager {

    public static void navegarWeb(){
        navigateTo(PropertyManager.getProperty("web.base.url"));
    }

    public static void clickShopButton() {
        click(HomeConstants.SHOP_BUTTON);
    }

    public static void clickHomeButton() {
        click(HomeConstants.HOME_BUTTON);
    }


    public static boolean checkFourthArrival() {
        boolean status = true;
        try {
            WebElement element = getElement(HomeConstants.ARRIVAL_IMAGE, String.valueOf(4));
        }
        catch (Exception e){
            status=false;
        }
        return status;
    }

    public static void checkArrivals(){
        List<WebElement> elements = new ArrayList<>();
        for (int i=1;i<=3;i++){
            elements.add(getElement(HomeConstants.ARRIVAL_IMAGE,String.valueOf(i)));
        }

        Assert.assertEquals(elements.size(),3);
        Assert.assertFalse(checkFourthArrival());

    }

    public static void clickImage(){click(HomeConstants.ARRIVAL_IMAGE,String.valueOf(1));}

    public static void clickAddBasket(){click(HomeConstants.ADD_BUTTON);}

    public static void checkMenuItem(){
        String items = getText(HomeConstants.MENU_ITEM);
        String price = getText(HomeConstants.PRICE_ITEM);
       Assert.assertEquals(items,"1 Item");
       Assert.assertEquals(price,"₹500.00");

    }
    public static void clickMenuItem(){
        click(HomeConstants.MENU_ITEM_BUTTON);
    }


    public static void checkTotals() {
        for (int i = 1; i <= 3; i++) {
            if (i == 1) {
                Assert.assertEquals(getAttribute(HomeConstants.TOTALS_SUBTOTALS, "data-title", String.valueOf(i)), "Subtotal");
            } else if (i == 2) {
                Assert.assertEquals(getAttribute(HomeConstants.TOTALS_SUBTOTALS, "data-title", String.valueOf(i)), "Tax");
            } else {
                Assert.assertEquals(getAttribute(HomeConstants.TOTALS_SUBTOTALS, "data-title", String.valueOf(i)), "Total");
            }
        }
    }
        public static void checkTotalisBigger(){
            Float subtotal = Float.valueOf(getText(HomeConstants.TOTALS_SUBTOTALS,String.valueOf(1)).substring(1));
            Float total = Float.valueOf(getText(HomeConstants.TOTALS_SUBTOTALS,String.valueOf(3)).substring(1));
             Assert.assertTrue(subtotal<total);
    }
    public static void clickCheckout(){click(HomeConstants.CHECK_OUT);}

    public static void detailsDisplayed(){
        WebDriver driver = DriverManager.getDriverInstance();
        Actions actions = new Actions(driver);
        actions.moveToElement(getElement(HomeConstants.BILLING_DETLS)).perform();
        Assert.assertTrue(getElement(HomeConstants.BILLING_DETLS).isDisplayed());
        actions.moveToElement(getElement(HomeConstants.ADD_INFO)).perform();
        Assert.assertTrue(getElement(HomeConstants.ADD_INFO).isDisplayed());
        actions.moveToElement(getElement(HomeConstants.ORDER_REVIEW)).perform();
        Assert.assertTrue(getElement(HomeConstants.ORDER_REVIEW).isDisplayed());
        actions.moveToElement(getElement(HomeConstants.PAYMENT_GATEWAY)).perform();
        Assert.assertTrue(getElement(HomeConstants.PAYMENT_GATEWAY).isDisplayed());

    }

    public static void fillDetails(String firstname,String lastname,String email,int phone,String country,String address,String city,String state, String postcode){
        WebDriver driver = DriverManager.getDriverInstance();
        Actions actions = new Actions(driver);
        setInput(HomeConstants.FIRST_NAME,firstname);
        setInput(HomeConstants.LAST_NAME,lastname);
        setInput(HomeConstants.EMAIL,email);
        setInput(HomeConstants.PHONE, String.valueOf(phone));
        getElement(HomeConstants.COUNTRY).sendKeys(country);
        driver.findElement(By.id("select2-results-1")).click();
        //driver.findElement(By.id("select2-drop-mask")).click();
        setInput(HomeConstants.ADDRESS,address);
        setInput(HomeConstants.CITY,city);
        getElement(HomeConstants.STATE).sendKeys(state);
        Select options = new Select(getElement(HomeConstants.STATE_OPTIONS));
        for(int i=1;i<options.getOptions().size();i++){
            if(getElement(HomeConstants.STATE_DROP,String.valueOf(i)).getText().equals(state)){
                getElement(HomeConstants.STATE_DROP,String.valueOf(i)).click();
                break;
            }
        }
        //driver.findElement(By.id("select2-results-2")).
        setInput(HomeConstants.POST_CODE,postcode);
    }
    public static void selectPayment(String payment) {
        for (int i = 1; i <= 4; i++) {
            String s = HomeConstants.PAYMENT_METHOD.replace("%s", String.valueOf(i));
            String text = getText(s + "/label");
            if (text == payment) {
                click(HomeConstants.PAYMENT_METHOD, String.valueOf(i));
            }
        }
    }

    public static void navigateCoupon(){
        WebDriver driver = DriverManager.getDriverInstance();
        Actions actions = new Actions(driver);
        actions.moveToElement(getElement(HomeConstants.ADD_COUPON)).click().perform();
        Assert.assertTrue(getElement(HomeConstants.COUPON_CODE).isDisplayed());

    }

    public static void placeOrder(){
        WebDriver driver = DriverManager.getDriverInstance();
        Actions actions = new Actions(driver);
        actions.moveToElement(getElement(HomeConstants.PLACE_ORDER)).click().perform();
        Assert.assertEquals(getText(HomeConstants.ORDER_RECEIVED),"Order Details");

    }


    }

