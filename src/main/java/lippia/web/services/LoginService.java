package lippia.web.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import lippia.web.constants.LoginConstants;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;



public class LoginService extends ActionManager {

    public static void clickAccountButton() {
        click(LoginConstants.ACCOUNT_BUTTON_NAME);
    }

    public static void enterEmailPass(String email,String password){
        setInput(LoginConstants.EMAIL_INPUT,email);
        setInput(LoginConstants.PASS_INPUT,password);
        click(LoginConstants.LOGIN_BUTTON);

    }

    public static void errorMessage(String username){
       String text= getText(LoginConstants.ERROR_MESSAGE);
        Assert.assertEquals(text,"Error: the password you entered for the username "+username+" is incorrect. Lost your password?");
    }

    public static void welcomeMessage(){
        Assert.assertTrue(getElement(LoginConstants.SIGN_IN_MESSAGE).isDisplayed());
    }

    public static void signOut(){
        click(LoginConstants.SIGN_OUT);
    }

    public static void userIsSignedOut(){
        WebDriver driver =  DriverManager.getDriverInstance();
        Actions action = new Actions(driver);
        waitVisibility(LoginConstants.EMAIL_INPUT);
        action.sendKeys(Keys.RETURN).perform();
        Assert.assertTrue(getElement(LoginConstants.EMAIL_INPUT).isDisplayed());

    }

    public static void registerEmailPass(String email){
        setInput(LoginConstants.EMAIL_REGISTER,email);
        click(LoginConstants.REGISTER_BUTTON);
    }

    public static void missingPass(){
       String text= getText(LoginConstants.ERROR_MESSAGE);
        Assert.assertEquals(text,"Error: Please enter an account password.");
    }

    public static void missingPassandEmail(){
        String text= getText(LoginConstants.ERROR_MESSAGE);
        Assert.assertEquals(text,"Error: Please provide a valid email address.");
    }

    public static void clickButton(String action){
        click(LoginConstants.ACCOUNT_BUTTON_NAME);
        if (action.equals("Account Details")){
            click(LoginConstants.ACC_DETAILS);
        } else if (action.equals("Logout")){
            click(LoginConstants.LOG_OUT);
        }

    }

    public static void checkChangePass(){
        Assert.assertEquals(getText(LoginConstants.CHANGE_PASS),"Password Change");
    }
}
