package lippia.web.constants;

public class LoginConstants {
    public static final String ACCOUNT_BUTTON_NAME = "xpath://*[@id='menu-item-50']";

    public static final String EMAIL_INPUT = "xpath://*[@id='username']";

    public static final String LOGIN_BUTTON = "xpath://*[@id='customer_login']/div[1]/form/p[3]/input[3]";

    public static final String PASS_INPUT = "xpath://*[@id='password']";

    public static final String ERROR_MESSAGE = "xpath://*[@id='page-36']/div/div[1]/ul/li";

    public static final String SIGN_IN_MESSAGE="xpath://*[@id='page-36']/div/div[1]/div/p[1]";

    public static final String SIGN_OUT="xpath://*[@id='page-36']/div/div[1]/div/p[1]/a";

    public static final String EMAIL_REGISTER="xpath://*[@id='reg_email']";

    public static final String REGISTER_BUTTON="xpath://*[@id='customer_login']/div[2]/form/p[3]/input[3]";

    public static final String ACC_DETAILS="xpath://*[@id='page-36']/div/div[1]/nav/ul/li[5]/a";

    public static final String CHANGE_PASS="xpath://*[@id='page-36']/div/div[1]/div/form/fieldset/legend";

    public static final String LOG_OUT="xpath://*[@id='page-36']/div/div[1]/nav/ul/li[6]/a";

}
