package lippia.web.constants;

public class ShopConstants {
    public static final String ADD_BASKET = "xpath://*[@id='content']/ul/li[7]/a[2]";

    public static final String MENU_ITEM = "xpath://*[@id='wpmenucartli']/a/span[1]";

    public static final String PRICE_ITEM = "xpath://*[@id='wpmenucartli']/a/span[2]";

    public static final String COUNTRY="xpath://*[@id='s2id_autogen1']";

    public static final String TAX="xpath://*[@id='order_review']/table/tfoot/tr[2]/td/span";



}

