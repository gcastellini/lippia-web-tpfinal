package lippia.web.constants;

public class HomeConstants {

    public static final String SHOP_BUTTON = "xpath://*[@id='menu-item-40']/a";
    public static final String HOME_BUTTON = "xpath://*[@id='content']/nav/a";

    public static final String ARRIVAL_IMAGE = "xpath://*[@id='themify_builder_content-22']/div[2]/div/div/div/div/div[2]/div[%s]";

    public static final String ADD_BUTTON="xpath://*[@id='product-160']/div[2]/form/button";

    public static final String MENU_ITEM="xpath://*[@id='wpmenucartli']/a/span[1]";
    public static final String PRICE_ITEM="xpath://*[@id='wpmenucartli']/a/span[2]";

    public static final String MENU_ITEM_BUTTON="xpath://*[@id='wpmenucartli']";

    public static final String TOTALS_SUBTOTALS="xpath://*[@id='page-34']/div/div[1]/div/div/table/tbody/tr[%s]/td";

    public static final String CHECK_OUT="xpath://*[@id='page-34']/div/div[1]/div/div/div/a";

    public static final String BILLING_DETLS="xpath://*[@id='customer_details']/div[1]";

    public static final String ADD_INFO="xpath://*[@id='customer_details']/div[2]";

    public  static final String ORDER_REVIEW="xpath://*[@id='order_review']";
    public static final String PAYMENT_GATEWAY="xpath://*[@id='payment']";

    //Billing Info

    public static final String FIRST_NAME="xpath://*[@id='billing_first_name']";

    public static final String LAST_NAME="xpath://*[@id='billing_last_name']";

    public static final String EMAIL="xpath://*[@id='billing_email']";

    public static final String PHONE="xpath://*[@id='billing_phone']";

    public static final String COUNTRY="xpath://*[@id='s2id_autogen1']";

    public static final String ADDRESS="xpath://*[@id='billing_address_1']";

    public static final String CITY="xpath://*[@id='billing_city']";

    public static final String STATE="xpath://*[@id='s2id_autogen2']";

    public static final String STATE_DROP="xpath://*[@id='select2-results-2']/li[%s]";

    public static final String STATE_OPTIONS="xpath://*[@id='billing_state']";


    public static final String POST_CODE="xpath://*[@id='billing_postcode']";

    public static final String PAYMENT_METHOD="xpath://*[@id='payment']/ul/li[%s]";

    public static final String ADD_COUPON="xpath://*[@id='page-35']/div/div[1]/div[2]/a";

    public static final String COUPON_CODE="xpath://*[@id='coupon_code']";

    public static final String PLACE_ORDER="xpath://*[@id='place_order']";

    public static final String ORDER_RECEIVED="xpath://*[@id='page-35']/div/div[1]/h2[2]";


}
