package lippia.web.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lippia.web.services.ShopService;

public class ShopSteps {

    @When("The client adds item to basket")
    public void clickAddBasket(){
        ShopService.clickAddBasket();
    }

    @And("The client sees item and price in Menu item")
    public void checkMenuItem() throws InterruptedException {
        ShopService.checkMenuItem();
    }

    @Then("The client checks tax for India is 2% and for (.*) is 5%")
    public void checkTax(String country) throws InterruptedException {
        ShopService.fillIndia();
        ShopService.fillCountry(country);

    }
}
