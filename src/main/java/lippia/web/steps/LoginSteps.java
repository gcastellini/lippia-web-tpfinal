package lippia.web.steps;

import com.crowdar.core.PageSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lippia.web.services.LoginService;

public class LoginSteps extends PageSteps {

    @When("The client clicks on My Account button")
    public void clickAccount(){
        LoginService.clickAccountButton();
    }

    @And("^The client enters email (.*) and password (.*)$")
    public void enterEmailPass(String email,String password){
        LoginService.enterEmailPass(email,password);
    }

    @Then("The client see error message for user (.*)")
    public void checkMessage(String username){
        LoginService.errorMessage(username);
    }

    @And("The client is logged in")
    public void login(){
        LoginService.welcomeMessage();
    }

    @And("The client signs out")
    public void signOut(){
        LoginService.signOut();
    }

    @Then("The client goes back and is not signed in")
    public void isSignedOut(){
        LoginService.userIsSignedOut();
    }

    @When("The client registers an email (.*) and no password")
    public void registerEmail(String email){
        LoginService.registerEmailPass(email);
    }

    @Then("An error message is displayed for email (.*)")
    public void missingPass(String email) {
        if (email == "") {
            LoginService.missingPassandEmail();
        } else {
            LoginService.missingPass();
        }
    }

    @When("The client clicks on My Account and (.*)")
    public void clickAccDetails(String button){
        LoginService.clickButton(button);
    }

    @Then("The client sees they can change the password")
    public void changePass(){
        LoginService.checkChangePass();
    }
}
