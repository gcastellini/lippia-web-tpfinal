package lippia.web.steps;

import com.crowdar.core.PageSteps;
import io.cucumber.java.en.*;
import lippia.web.services.HomeService;

public class HomeSteps extends PageSteps {

    @Given("The client is in Practice page")
    public void navigate() {
        HomeService.navegarWeb();
    }

    @When("^The client clicks on Shop Button")
    public void shop() {
        HomeService.clickShopButton();
    }

    @And("^The client clicks on Home Button")
    public void home() {
        HomeService.clickHomeButton();
    }

    @And("The client clicks on Arrival Image")
    public void clickArrival() {
        HomeService.clickImage();
    }

    @And("The client checks there are three arrivals")
    public void arrivals() {
        HomeService.checkArrivals();
    }

    @When("The client clicks on Add To Basket button")
    public void addBasket() {
        HomeService.clickAddBasket();
    }

    @And("The client verifies the book is in Menu item with price")
    public void text() {
        HomeService.checkMenuItem();
    }

    @And("The client clicks on the Menu Item")
    public void clickMenuItem() {
        HomeService.clickMenuItem();
    }

    @And("The client find total and subtotal")
    public void findTotal() {
        HomeService.checkTotals();
    }

    @And("The client checks total is bigger that subtotal")
    public void checkTotalBigger() {
        HomeService.checkTotalisBigger();
    }

    @And("The client clicks on Proceed to Checkout")
    public void checkOut() {
        HomeService.clickCheckout();
    }

    @Then("The client can see Billing Details,Order Details,Additional details and Payment gateway details")
    public void seeDetails() {
        HomeService.detailsDisplayed();
    }

    @And("The client can fill in his details (.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*)")
    public void fillDetails(String firstname, String lastname, String email, int phone, String country, String address, String city, String state, String postcode, String payment) {
        HomeService.fillDetails(firstname, lastname, email, phone, country, address, city, state, postcode);
        HomeService.selectPayment(payment);
    }

    @And("The client tries to (.*)")
    public void addCoupon(String button) {
        if (button.equals("add coupon")) {
            HomeService.navigateCoupon();
        } else {
            HomeService.placeOrder();

        }
    }


}



